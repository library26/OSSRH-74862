package io.gitlab.library26.service;

import java.math.BigDecimal;

public class ArithmeticOpsService {


    public BigDecimal add(BigDecimal a, BigDecimal b){

        return a.add(b);

    }

    public BigDecimal sub(BigDecimal a, BigDecimal b){

        return a.subtract(b);

    }

    public BigDecimal mul(BigDecimal a, BigDecimal b){

        return a.multiply(b);

    }

    public BigDecimal div(BigDecimal a, BigDecimal b) throws Exception {

        try {
            return a.divide(b);
        }catch (Exception e){
            throw new Exception(e.getMessage(),e.getCause());
        }
    }
}
